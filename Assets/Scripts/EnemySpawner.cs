﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemySpawner : MonoBehaviour
{
    public float spawnTime;
    public GameObject enemy;

	// Use this for initialization
	void Start ()
    {
        InvokeRepeating("Spawn", spawnTime, spawnTime);	
	}

    void Spawn()
    {
        for (int i = 0; i < 10; i++)
        {
            Instantiate(enemy, RandomPosition(), Quaternion.identity);
        }             
    }

    Vector3 RandomPosition()
    {
        Vector3 position = Random.insideUnitSphere * 50;
        position += transform.position;
        position.y = 1.2f;
        NavMeshHit hit;
        NavMesh.SamplePosition(position, out hit, 50, 1);
        Vector3 finalPosition = hit.position;
        return finalPosition;
    }
}
