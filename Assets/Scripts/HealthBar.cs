﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{

    private RectTransform Bar;
    private float playerMaxHealth;
    private float playerHealth;

	// Use this for initialization
	void Start ()
    {
        Bar = GetComponent<RectTransform>();
        playerHealth = GameObject.FindGameObjectWithTag("Player").GetComponent<CustomCharacterController>().currentHealth;
        playerMaxHealth = GameObject.FindGameObjectWithTag("Player").GetComponent<CustomCharacterController>().playerHealth;
    }
	
	// Update is called once per frame
	void Update ()
    {
        playerHealth = GameObject.FindGameObjectWithTag("Player").GetComponent<CustomCharacterController>().currentHealth;
        Bar.localScale = new Vector3(playerHealth / playerMaxHealth,1,1);
	}
}
