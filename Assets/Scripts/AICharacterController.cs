﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class AICharacterController : MonoBehaviour
{

    public float moveSpeed = 3f;
    public float maxSpeed = 10f;
    public float playerDamage = 1f;

    private GameObject player;

	// Use this for initialization
	void Start ()
    {
        player = GameObject.FindGameObjectWithTag("Player");
	}
	
	// Update is called once per frame
	void Update ()
    {
        transform.LookAt(player.transform);

        if(GetComponent<Rigidbody>().velocity.magnitude < maxSpeed)
            GetComponent<Rigidbody>().AddForce(transform.forward * moveSpeed);
	}

    void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.CompareTag("Sword"))
            Destroy(transform.gameObject);
    }
}
