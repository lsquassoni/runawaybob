﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CustomCharacterController : MonoBehaviour
{
    public float maxSpeed = 7f;
    public float force = 8f;
    public float jumpSpeed = 5f;
    public float playerHealth = 10f;

    public float currentHealth { get; set; }

    private int state = 0;
    private bool grounded = false;
    private float jumpLimit = 0;
    private Camera mainCamera;
    private Rigidbody rigidBody;
    private GameObject Sword;
    
    void Awake()
    {
        rigidBody = GetComponent<Rigidbody>();
        rigidBody.freezeRotation = true;
        currentHealth = playerHealth;
        Sword = GameObject.FindGameObjectWithTag("Sword");
    }        
    
    void OnCollisionEnter()
    {
        state++;
        if(state > 0)
        {
            grounded = true;
        }
    }

    void OnCollisionExit()
    {
        state--;
        if(state < 1)
        {
            grounded = false;
            state = 0;
        }
    }

    public virtual bool jump
    {
        get
        {
            return Input.GetButtonDown("Jump");
        }
    }

    public virtual float horizontal
    {
        get
        {
            return Input.GetAxis("Horizontal") * force;
        }
    }

    public virtual float vertical
    {
        get
        {
            return Input.GetAxis("Vertical") * force;
        }
    }

    void Start()
    {
        mainCamera = Camera.main;
    }

    void FixedUpdate()
    {
        if(GetComponent<Rigidbody>().velocity.magnitude < maxSpeed && grounded == true)
        {
            rigidBody.AddForce(transform.rotation * transform.forward * vertical);
            rigidBody.AddForce(transform.rotation * -transform.right * horizontal);
        }

        Vector3 movementDir = new Vector3(horizontal, 0, vertical);
        transform.localRotation = Quaternion.LookRotation(movementDir);
        mainCamera.gameObject.transform.position = new Vector3(transform.position.x, transform.position.y + 4f, transform.position.z - 10f);
        Sword.transform.position = transform.position + transform.forward;
        Sword.transform.rotation = transform.localRotation;
    }

    void OnTriggerEnter(Collider other)
    {
        Debug.Log("Collision");
        if(other.gameObject.GetComponent<SphereCollider>())
        {
            float playerDamage = other.gameObject.GetComponent<AICharacterController>().playerDamage;
            currentHealth -= playerDamage;
            Debug.Log(currentHealth);
        }
    }
}

